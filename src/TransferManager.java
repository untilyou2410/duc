
import java.util.Random;
import java.util.Scanner;

public class TransferManager {

    Thread a;
    Thread b;
    double transferCount;
    CustomerAccount x;
    CustomerAccount y;

    public TransferManager() {
        x = new CustomerAccount(5000, "x");
        y = new CustomerAccount(2000, "y");
    }

    //giam tien
    public void initThreadA() {
        a = new Thread() {
            @Override
            public void run() {
                Random z = new Random();
                Scanner in = new Scanner(System.in);
                while (true) {               
                    System.out.println(x.toString());
                    transferCount = z.nextDouble() * 1000;
                    //xem so tien tru di nho hon trong tai khoan
                    x.setBalance(x.getBalance() - transferCount );
                    try {
                        sleep(2000);
                    } catch (Exception e) {
                    }
                }
            }
        };

    }

    //cong  tien
    public void initThreadB() {
        b = new Thread() {
            public void run() {
                while (true) {
                    System.out.println(y.toString());
                    y.setBalance(y.getBalance() + transferCount);
                     try {
                        sleep(2000);
                    } catch (Exception e) {
                    }
                }
            }
        };

    }

}
