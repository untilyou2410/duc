/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author anhnb
 */
public class CustomerAccount {
    private double balance;
    private String accName;

    public CustomerAccount() {
    }

    public CustomerAccount(double balance, String accName) {
        this.balance = balance;
        this.accName = accName;
    }
    
    
    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getAccName() {
        return accName;
    }

    public void setAccName(String accName) {
        this.accName = accName;
    }

    @Override
    public String toString() {
        return "CustomerAccount{" + "balance=" + balance + ", accName=" + accName + '}';
    }
    
    
    
}
